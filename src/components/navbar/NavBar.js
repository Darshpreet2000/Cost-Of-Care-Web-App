import {
    Nav,
    NavLink,
    NavMenu,
    NavBtn,
    NavBtnLink,
    LogoLink,OpenBars,CloseBars
  } from './NavBarElements';
  import React, { useState } from 'react';
const NavBar = () => {
    const [open, setOpen] = useState(false)
    return (
        
        <Nav>
             <LogoLink to='/'>
             <img src={'/librehealth.png'} alt='logo' height ='60px' />
          </LogoLink>
           {open?<CloseBars  open={open} onClick={() => setOpen(!open)}/>:
           <OpenBars  open={open} onClick={() => setOpen(!open)}/>
           }
            <NavMenu  open={open}>
                
            <NavLink   exact to="/" activeStyle>
               Home
               </NavLink>
               
            <NavLink  to="/compare-hospitals" activeStyle>
               Compare Hospitals
               </NavLink>
            <NavLink   to="/about" activeStyle>
               About Us
               </NavLink>
            </NavMenu>
          
        </Nav>
        
    )
}

export default NavBar
