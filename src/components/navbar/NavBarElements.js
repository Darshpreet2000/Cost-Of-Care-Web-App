import { FaBars, FaTimes } from 'react-icons/fa';
import { NavLink as Link } from 'react-router-dom';
import styled from 'styled-components';

export const Nav = styled.nav`
  height: 80px;
  box-shadow: 0 2px 4px 0 rgba(0,0,0,.2);
  display: flex;
  justify-content: space-between;
  padding: 0.5rem calc((100vw - 1000px) / 3);
  z-index: 250;
  position: sticky;
   top: 0;
  background:white;
  /* Third Nav */
  /* justify-content: flex-start; */
`;

export const LogoLink = styled(Link)`
  color: #000;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 1rem;
  height: 100%;
  cursor: pointer;
  &.active {
    color: #FF7F50;
  }
`;

export const NavLink = styled(Link)`
  color: #000;
  display: flex;
  align-items: center;
  text-decoration: none;
  padding: 0 2rem;
  height: 100%;
  cursor: pointer;
  @media screen and (max-width: 768px) {
    height: 20%;
  }
  &:hover {
  transform: scale(1.05); 
}
  &.active {
    color: #FF7F50;
  }
  }
`;

export const OpenBars = styled( FaBars)`
  display: none;
  color: #000;
  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;
export const CloseBars = styled( FaTimes)`
  display: none;
  color: #000;
  @media screen and (max-width: 768px) {
    display: block;
    position: absolute;
    top: 0;
    right: 0;
    transform: translate(-100%, 75%);
    font-size: 1.8rem;
    cursor: pointer;
  }
`;

export const NavMenu = styled.ul`
list-style: none;
  display: flex;
  justify-content:flex-end
  flex-flow: row nowrap;
  @media (max-width: 768px) {
    height: 100%;
   background: white;
   box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.5);
   position: fixed;
   right: 0;
   width:  300px;
    z-index: 200;
   transition: transform 0.3s ease-out;
    flex-flow: column nowrap;
    transform: ${({ open }) => open ? 'translateX(0)' : 'translateX(100%)'};
    top: 80px;
  }
`;

export const NavBtn = styled.nav`
  display: flex;
  align-items: center;
  margin-right: 24px;
  /* Third Nav */
  /* justify-content: flex-end;
  width: 100vw; */
  @media screen and (max-width: 768px) {
    display: none;
  }
`;

export const NavBtnLink = styled(Link)`
  border-radius: 4px;
  background: #256ce1;
  padding: 10px 22px;
  color: #fff;
  outline: none;
  border: none;
  cursor: pointer;
  transition: all 0.2s ease-in-out;
  text-decoration: none;
  margin-left: 24px;
  &:hover {
    transition: all 0.2s ease-in-out;
    background: #fff;
    color: #010606;
  }
`;