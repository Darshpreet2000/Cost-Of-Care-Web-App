import { BrowserRouter,Switch,Route } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import NavBar from './components/navbar/NavBar';
import About from './pages/About';
import CompareHospitals from './pages/CompareHospitals';
import Home from './pages/home/Home';
import {ThemeProvider,createMuiTheme} from "@material-ui/core"
import React , { useEffect }from 'react'
import {orange} from '@material-ui/core/colors'
import 'fontsource-roboto'
import HospitalList from './pages/selectHospitals/HospitalList';
import SearchProcedure from './pages/searchProcedures/SearchProcedure';

const theme = createMuiTheme({
  palette: {
    primary: { 
      main: orange[400] 
    },
    secondary: {
      main: orange[400] 
    }
    
  },

})

function App() {
  
  return(
    <div className="application">
    
    <Helmet>
     <meta charSet="utf-8" />
     <title>Cost of Care</title>
    <meta name="description" content="Cost of Care by LibreHealth, Compare prices of medical procedures of US Hospitals" />
    <meta name="theme-color" content="#FFA500" />
    </Helmet>
      <ThemeProvider theme={theme}>
       <BrowserRouter>
        <NavBar/>
         <Switch>
        <Route path='/' exact component={Home}/>
          
        <Route path='/hospital-list' exact component={HospitalList}/>

        <Route path='/search-procedure' exact component={SearchProcedure}/>

          <Route path='/about' exact component={About}/>
          
          <Route path='/compare-hospitals' exact component={CompareHospitals}/>
         </Switch>
       </BrowserRouter>
       </ThemeProvider>
       </div>
    )
}
export default App;