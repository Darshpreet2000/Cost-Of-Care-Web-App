import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import {NavLink} from "react-router-dom";
import InputAdornment from "@material-ui/core/InputAdornment";
import Box from '@material-ui/core/Box';
import Paper from '@material-ui/core/Paper';
import InputBase from '@material-ui/core/InputBase';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import SearchIcon from '@material-ui/icons/Search';
import DirectionsIcon from '@material-ui/icons/Directions';
import {Button,Container, Typography} from "@material-ui/core";
import SearchProcedureListTile from './SearchProcdureListTile';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme) => ({
  root: {

  },
  textField: {
    fontSize: 20, //works!
  },
}));

export default function SearchProcedure(props) {
  const classes = useStyles();
  console.log("Working fine")
  const [originalState, setoriginalState] = React.useState(props.location.state.data);
 

  const [open, setOpen] = React.useState(false);
  const [listOfData, setListOfData] = React.useState([]);

  const handleClick = () => {
    setOpen(true);
  };
  
  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }
  
    setOpen(false);
  };

  function handleChange(e){
    console.log(e.target.value)
      const search=e.target.value
      var newListOfData=[]
      var re = new RegExp(search, 'i');
      if(search.length!=0){
      originalState.forEach(function(hospital){
        
        hospital.arr.forEach(function(entry){                       
               if(entry.Description.match(re)){
                      newListOfData.push(
                          {
                          "Description": entry.Description,
                          
                          "Charge":entry.Charge,
                          
                          "Category":entry.Category,
                          
                          "Hospital":hospital.name,

                          }

                      )
                } 
          })
     
   })
  }      
         if(newListOfData.length===0)
          setOpen(true)
          else
          setOpen(false)
         console.log(newListOfData)
          setListOfData(newListOfData)
  }
  return (
    <Box m={2} pt={1} >
  
            <h2 style={{'text-align': 'center'}}>
           Enter Procedure Name to Compare Prices
            </h2>
      <div>
      <Paper elevation={2}>
        <TextField
          id="outlined-full-width"
          label="Start typing procedure to search"
          onChange={handleChange}
          
          placeholder="Search Here"
          fullWidth
          margin="normal"
          variant="outlined"
          InputProps={{
            classes: {
              input: classes.textField,
            },
    endAdornment: (
      <InputAdornment>
        <IconButton>
          <SearchIcon />
        </IconButton>
      </InputAdornment>
    )
  }}>
      </TextField>
      </Paper>
      <Box m={2} pt={1} />
        <SearchProcedureListTile  list={listOfData}/>
      </div>
      <div className="div">
         <Snackbar open={open} autoHideDuration={6000} onClose={handleClose} >
        <Alert severity="info" onClose={handleClose}>
         No Results Found, Try To Change Search Query
        </Alert>
      </Snackbar>         
    </div>
    </Box>
 
  );
}