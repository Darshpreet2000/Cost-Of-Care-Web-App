import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import IconButton from '@material-ui/core/IconButton';
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import FolderIcon from '@material-ui/icons/Folder';
import DeleteIcon from '@material-ui/icons/Delete';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';

import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
function Alert(props) {
  return <MuiAlert elevation={6} variant="filled" {...props} />;
}
const useStyles = makeStyles((theme) => ({
  root: {
  },
  demo: {
    backgroundColor: theme.palette.background.paper,
  },
  title: {
   
  },
  procedure: {
     
  },
  orange:{
    color:"#FFA500"
  },
  price:{
    fontWeight: "bold",
  }
}));

function generate(element) {
  return [0, 1, 2].map((value) =>
    React.cloneElement(element, {
      key: value,
    }),
  );
}


export default function SearchProcedureListTile(props) {
  const classes = useStyles();
  const [dense, setDense] = React.useState(false);
  const [secondary, setSecondary] = React.useState(false);
  return (
    
    <Grid container spacing={2}>
              {props.list.map(function(obj){
             return(
                <Grid item xs={12} md={4}>
                <Card className={classes.root}>
      <CardContent>
      <Grid container spacing={2}>
    
      <Grid item xs={8} md={8}>
        <h4>{obj.Description}</h4>
         </Grid>     
         
      <Grid item xs={4} md={4}>
           <Typography className={classes.price}  gutterBottom>
           {"$ "+obj.Charge}
        </Typography>
        </Grid>
    </Grid>
        <Typography className={classes.orange} gutterBottom >
        {obj.Category}
        </Typography>
        <Typography className={classes.pos} >
        {obj.Hospital}
        </Typography>
      </CardContent>
    </Card>
    </Grid>

             )
              })}
          
        </Grid>
      
        
     
  );
            
}