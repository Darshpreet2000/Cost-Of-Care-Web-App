import React , {useState} from 'react'
import {Button,Container} from "@material-ui/core"
import CountrySelect from './CountrySelect';
import { makeStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import FormLabel from '@material-ui/core/FormLabel';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import RadioGroup from '@material-ui/core/RadioGroup';
import Radio from '@material-ui/core/Radio';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import { Height } from '@material-ui/icons';
import {NavLink} from "react-router-dom";
import Typography from '@material-ui/core/Typography';
import MuiAlert from '@material-ui/lab/Alert';
import { render } from '@testing-library/react';
import Snackbar from '@material-ui/core/Snackbar';
import { useHistory } from 'react-router-dom'
const useStyles = makeStyles((theme) => ({
  root: {
    'min-height':' calc(100vh - 110px)'
  },
  margin: {
    margin: theme.spacing(1),
  },
 
  control: {
    padding: theme.spacing(2),
    textAlign:'center',
    'min-height':'400px'
  },
}));

const Home = () => {
  const history=useHistory()
  const classes = useStyles();
  const [stateName, setstateName] = useState("");
  const [displaySnackbar, setDisplaySnackbar] = useState(false);
  
  function changeState(evt,value){
    console.log(stateName)  
    if(value==null){
        setstateName("")
      }
      else
       setstateName(value)
       
  }
  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }


function handleClick(){
    console.log(stateName)
      if(stateName.length==0){
        setDisplaySnackbar(true);  
      }
      else{
         history.push( {
            pathname: '/hospital-list',
           state: { selectedStateName: stateName}
          });
      }
  }
  function handleClose(){
     setDisplaySnackbar(false)
  }
    return (
        <Box m={2} pt={1} >
        <Grid container spacing={2} 
          container
  alignItems="center"
  justify="center"
         align="center"
         >
       
          <Grid item xs={12} sm={8} >
          
            <div className="slides_column" >
               <h1 style={{fontSize:45}}> Cost Of Care</h1>
                <br/> <br/><br/>
                <h3>Compare charges for common procedures at over 3,300 hospitals,
sourced from government data and user reported bills.</h3>
            </div>
            </Grid> 
  
          <Grid item xs={12} sm={4}>
          <Paper className={classes.control} elevation={5}>
           <div className="location_column"  style={{'padding-top': '10px'}} >
                <h2>Choose Location</h2>
                <div className="country-select"  style={{'padding-top': '50px'}} >
                  <CountrySelect changeStateFunction={changeState}/>
                  <div className="next_button"  style={{ 'padding-top': '40px'}}>
                  <Button  onClick={handleClick} className={classes.margin}  size="large" variant="contained" color="primary"  >
                 Find Hospitals
                 </Button>
                 <Snackbar open={displaySnackbar} autoHideDuration={600}>
  <Alert  severity="error"  onClose={handleClose}>
Please select your state  </Alert>
</Snackbar>
                 
                  </div>
                </div>
                </div>
           </Paper>
          </Grid>
         
        </Grid>
        </Box>
    )
  }
  

export default Home