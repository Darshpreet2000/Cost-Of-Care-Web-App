import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import Checkbox from '@material-ui/core/Checkbox';
import Avatar from '@material-ui/core/Avatar';
import Grid from '@material-ui/core/Grid';
import {Button,Container, Typography} from "@material-ui/core"

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  listItemText:{
    fontSize:'1.5em',//Insert your required size
  },
  
}));

export default function CheckboxListSecondary(props) {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(props.hospitalNamesList);
  const [listOfData, setListOfData] = React.useState([]);
  const apiUrl="https://raw.githubusercontent.com/Darshpreet2000/API/master/"

  const handleToggle = (value,tick) =>async () =>  {
    const currentIndex = checked.findIndex(x => x.name ===value);
   

    var newChecked=[...checked];
    var newList=[...listOfData];

    if(tick!=true){
    
       newChecked[currentIndex].checked=true;
    }
    else{
      newChecked[currentIndex].checked=false;     
    }
     if(newChecked[currentIndex].checked){
      const url=apiUrl+props.stateName+"/"+newChecked[currentIndex].name+".json";
      fetch(url)
       .then((response) => response.json())
       .then((responseJson) => {     
         newList.push({"name":newChecked[currentIndex].name,"arr":responseJson})
         props.hospitalFunction(newList)
         setListOfData(newList)    
        })
    } 
    else{
      //remove it from list
     console.log(newChecked[currentIndex].name)
      const curIndex = newList.findIndex(x => x.name ===newChecked[currentIndex].name);
     // console.log(curIndex)
     newList.splice(curIndex, 1); 
     props.hospitalFunction(newList)
      setListOfData(newList)  
    }
   
    setChecked(newChecked);
  };

  return ( 
    <List style={{'list-style-type': 'none'}}>   
    
    <Grid container spacing={2}> 
    
      {checked.map(function(obj){
        return (
            <Grid item xs={12} md={4}>
          <ListItem key={obj.name} button onClick={handleToggle(obj.name,obj.checked)} >
            <Typography  variant="h6">
            {` ${obj.name}`}
            </Typography>
            <ListItemSecondaryAction>
              <Checkbox
                edge="end"
                onChange={handleToggle(obj.name,obj.checked)}
                checked={obj.checked}
                inputProps={{ 'aria-labelledby': 'labelId' }}
              />
            </ListItemSecondaryAction>
          </ListItem>
          </Grid>
        );
     

      })}
      
 </Grid>
      </List>
  );
}