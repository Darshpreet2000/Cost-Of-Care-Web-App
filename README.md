## Cost of Care Web App

This web app is made with React, it allows user to compare prices of medical procedures of US Hospitals.

### Hosted on firebase

#### [cost-of-care.web.app](http://cost-of-care.web.app/)

### ScreenShots

<img src="/screenshots/home.png" align="top">

<img src="/screenshots/select_hospital.png" align="top">

<img src="/screenshots/compare.png" align="top">
